using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleController : MonoBehaviour
{
    [SerializeField]
    private Camera _head;
    [SerializeField]
    private float _movementSpeed = 10;
    [SerializeField]
    private float _lookAroundSpeed = 5;
    [SerializeField]
    private float _sprintSpeed = 15;
    [SerializeField]
    private float _jumpStrength = 4000;
    [SerializeField]
    private float _jumpTimeout = .75f;
    [SerializeField]
    private float _minHeadPitch = -90f;
    [SerializeField]
    private float _maxHeadPitch = 90f;
    [SerializeField]
    private float _maxDistanceFromGroundToJump = .05f;

    private Rigidbody _rb;
    private PlayerInputActions _inputActions;
    CapsuleCollider collider;
    private bool _isPlayerOnGround = true;
    private float _targetPitch;
    private float _lastJumpTime = 0f;
    void Start()
    {
        if (_head == null)
        {
            throw new System.ArgumentNullException("No camera assigned to player.");
        }

        _rb = GetComponent<Rigidbody>();
        if (_inputActions == null)
        {
            _inputActions = new PlayerInputActions();
        }
        if (!_inputActions.OnFoot.enabled)
        {
            _inputActions.OnFoot.Enable();
        }
        _inputActions.OnFoot.Jump.performed += Jump_performed;
        collider = GetComponent<CapsuleCollider>();
    }

    private void Jump_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (_isPlayerOnGround && _lastJumpTime + _jumpTimeout < Time.time)
        {
            _rb.AddForce(Vector3.up * _jumpStrength);
            _lastJumpTime = Time.time;
        }

    }

    private void FixedUpdate()
    {
        RaycastHit hit;
        _isPlayerOnGround = Physics.Raycast(transform.position, -transform.up, out hit, collider.height / 2 + _maxDistanceFromGroundToJump);
        ProcessMovement();
    }

    void Update()
    {
        ProcessRotation();
    }

    private void ProcessRotation()
    {
        Vector2 lookAround = _inputActions.OnFoot.LookAround.ReadValue<Vector2>();
        RotateHead(lookAround);
        RotateBody(lookAround);
    }

    private void RotateHead(Vector2 lookAround)
    {
        _targetPitch += -lookAround.y * Time.unscaledDeltaTime * _lookAroundSpeed;
        _targetPitch = ClampAngle(_targetPitch, _minHeadPitch, _maxHeadPitch);
        _head.transform.localRotation = Quaternion.Euler(_targetPitch, 0f, 0f);
    }

    private void RotateBody(Vector2 lookAround)
    {
        Vector3 rbRotation = new Vector3(0, _rb.rotation.eulerAngles.y + lookAround.x * Time.unscaledDeltaTime * _lookAroundSpeed, 0);
        _rb.MoveRotation(Quaternion.Euler(rbRotation));
    }

    private void ProcessMovement()
    {
        Vector2 inputVector = _inputActions.OnFoot.Movement.ReadValue<Vector2>();
        Vector3 rigidBodyPosition = _rb.position;
        float currentMoveSpeed = _inputActions.OnFoot.Sprint.IsPressed() ? _sprintSpeed : _movementSpeed;
        Vector3 newPosition = rigidBodyPosition + currentMoveSpeed * Time.unscaledDeltaTime * (transform.forward * inputVector.y + transform.right * inputVector.x);
        _rb.MovePosition(newPosition);

    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        // Bezwstydna podpierdolka z FPS ze StandardAssets, ale mia�em ju� do�� walki z ograniczeniem rotacji Quaterniona
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

    public void OnDisable()
    {
        if (_inputActions == null)
        {
            _inputActions = new PlayerInputActions();
        }
        _inputActions.OnFoot.Disable();
    }

    public void OnEnable()
    {
        if(_inputActions == null)
        {
            _inputActions = new PlayerInputActions();
        }
        if (!_inputActions.OnFoot.enabled)
        {
            _inputActions.OnFoot.Enable();
        }
    }
}
