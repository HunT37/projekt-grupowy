using System;

public static class GameUtils {
    public static string EnumValueName(Enum value, string delimiter = "") {
        return SplitCamelCase(Enum.GetName(value.GetType(), value), delimiter);
    }

    public static string SplitCamelCase(string str, string delimiter = "") {
        string result = "";
        for (int i = 0; i < str.Length; i++) {
            if (char.IsUpper(str[i]) && i != 0) {
                result += delimiter;
            }

            result += str[i];
        }
        return result;
    }
}
