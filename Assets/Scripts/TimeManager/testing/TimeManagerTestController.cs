using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TimeManager))]
public class TimeManagerTestController : MonoBehaviour
{

    [SerializeField]
    private float _slowDownTimeStep = .25f;
    [SerializeField]
    private float _speedUpTimeStep = .25f;
    [SerializeField]
    private float _speedUpTimeTo = 2f;
    [SerializeField]
    private float _slowDownTimeTo = .5f;
   
    private TimeManager timeManager;

    private TimeManagementKeymap keymap;

    void Start()
    {
        timeManager = GetComponent<TimeManager>();
        keymap = new TimeManagementKeymap();
        keymap.TimeControl.Enable();
        keymap.TimeControl.SlowDownTime.performed += SlowDownTime_performed;
        keymap.TimeControl.SpeedUpTime.performed += SpeedUpTime_performed;
        keymap.TimeControl.ToggleFreezeTime.performed += ToggleFreezeTime_performed;
        keymap.TimeControl.StepSpeedUpTime.performed += StepSpeedUpTime_performed;
        keymap.TimeControl.StepSlowDownTime.performed += StepSlowDownTime_performed;
    }

    private void StepSlowDownTime_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        timeManager.SlowDownTimeBy(_slowDownTimeStep);
    }

    private void StepSpeedUpTime_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        timeManager.SpeedUpTimeBy(_speedUpTimeStep);
    }

    private void ToggleFreezeTime_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        timeManager.ToggleFreezeTime();
    }

    private void SpeedUpTime_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        timeManager.SetTimescaleTo(_speedUpTimeTo);
    }

    private void SlowDownTime_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        timeManager.SetTimescaleTo(_slowDownTimeTo);    
    }

    private void OnDisable()
    {
        if (keymap != null && keymap.TimeControl.enabled)
        {
            keymap.Disable();
        }
    }

}
